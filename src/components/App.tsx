import React from 'react';
import './App.css';
import MainPage from "./main-page/MainPage";

const App: React.FC = () => {
  return <MainPage/>
}

export default App;
